default: env-test run/driver.run
	@make -f test.mk OUT=out2
	@make -f test.mk OUT=out1
	@printf "Docker Version: %s\n" "$(shell docker --version)"
	@printf "Docker Image Store: %s\n" "$(shell docker info -f '{{ .DriverStatus }} ')"
	@for f in out1 out2; do \
		printf "%-35s" "$${f} - docker save archive hash: "; \
		sha256sum $${f}/dockersave.tar | awk '{ print $$1 }'; \
	done
	@for f in out1 out2; do \
		printf "%-35s" "$${f} - docker buildx archive hash: "; \
		sha256sum $${f}/buildx.tar | awk '{ print $$1 }'; \
	done
	@for f in out1 out2; do \
		printf "%-35s" "$${f} - docker save digest: "; \
		tar -xf $${f}/dockersave.tar index.json -O | jq -r '.manifests[].digest | sub("sha256:";"")'; \
	done
	@for f in out1 out2; do \
		printf "%-35s" "$${f} - docker buildx digest: "; \
		tar -xf $${f}/buildx.tar index.json -O | jq -r '.manifests[].digest | sub("sha256:";"")'; \
	done
	@diffoscope --exclude-directory-metadata=yes out1 out2

.PHONY: clean
clean:
	docker container rm -f $$(cat $(OUT)/registry.id)
	docker buildx rm digest-test
	rm -rf out1 out2

.PHONY: env-test
env-test:
	which jq
	which docker

run:
	mkdir $@

run/registry.id: run
	docker container create \
		--publish 5000:5000 \
		--restart always \
		--name test-registry \
		registry:2 \
		> $@

run/registry.run: run/registry.id
	docker container start $$(cat run/registry.id)
	touch $@

run/driver.run: run
	docker buildx create \
		--name digest-test \
		--driver docker-container \
		--node digest-test
	docker buildx use digest-test
	touch $@
