OUT =: $(OUT)

default: all

.PHONY: all
all: $(OUT)/dockersave.tar $(OUT)/buildx.tar $(OUT)/metadata.json
	touch -hcd "@0" $(OUT)/*
	sha256sum $(OUT)/dockersave.tar
	sha256sum $(OUT)/buildx.tar

$(OUT):
	mkdir -p $(OUT)

$(OUT)/Dockerfile $(OUT)/noopfile: $(OUT)
	printf "from scratch\nADD noopfile ." > $(OUT)/Dockerfile
	printf "1" > $(OUT)/noopfile
	touch -hcd "@0" $(OUT)/noopfile

$(OUT)/buildx.tar $(OUT)/buildx_metadata.json: $(OUT)/Dockerfile
	DOCKER_BUILDKIT=1 \
	SOURCE_DATE_EPOCH=1 \
	/bin/docker buildx build \
		--tag local/digest-test \
		--no-cache \
		--metadata-file $(OUT)/metadata.json \
		--output type=oci,force-compression=true,name=local/digest-test,dest=$(OUT)/buildx.tar \
		$(OUT)

$(OUT)/dockersave.tar: $(OUT)/Dockerfile
	DOCKER_BUILDKIT=1 \
	SOURCE_DATE_EPOCH=1 \
	/bin/docker buildx build \
		--tag local/digest-test \
		--no-cache \
		--load \
		$(OUT)
	docker save local/digest-test -o $@
